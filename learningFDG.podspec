#
# Be sure to run `pod lib lint learningFDG.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
# 组件名称
  s.name             = 'learningFDG'
  # 组件版本号
  s.version          = '0.1.0'
  # 组件简介
  s.summary          = 'A short description of learningFDG.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!
# 组件详细描述，要比简介字数多一些
  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC
  # 组件仓库
  s.homepage         = 'https://github.com/fdg/learningFDG'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  # 设置许可
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  # 组件作者
  s.author           = { 'fdg' => '1399679714@qq.com' }
  # 组件仓库源,表示在哪可以找到组件工程(支持 git、svn、http 服务器)
  s.source           = { :git => 'https://github.com/fdg/learningFDG.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'learningFDG/Classes/**/*'
  
  # s.resource_bundles = {
  #   'learningFDG' => ['learningFDG/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
