# learningFDG

[![CI Status](https://img.shields.io/travis/fdg/learningFDG.svg?style=flat)](https://travis-ci.org/fdg/learningFDG)
[![Version](https://img.shields.io/cocoapods/v/learningFDG.svg?style=flat)](https://cocoapods.org/pods/learningFDG)
[![License](https://img.shields.io/cocoapods/l/learningFDG.svg?style=flat)](https://cocoapods.org/pods/learningFDG)
[![Platform](https://img.shields.io/cocoapods/p/learningFDG.svg?style=flat)](https://cocoapods.org/pods/learningFDG)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

learningFDG is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'learningFDG'
```

## Author

fdg, 1399679714@qq.com

## License

learningFDG is available under the MIT license. See the LICENSE file for more info.
